#!/usr/bin/env python3

import pika
from flask import Flask, request
import json

app = Flask(__name__)

@app.route('/rabbitmq', methods=['POST'])
def put_message_on_queue():
    print("inside")
    content = request.get_json()
    print(content)
    add_to_the_queue( content )
    return "Ok", 200


def add_to_the_queue( message, queue="queue"):
    credentials = pika.PlainCredentials( username= "user", password = "password")
    connection = pika.BlockingConnection(
                pika.ConnectionParameters(
                    host='rabbit',
                    credentials = credentials
                )
             )
    channel = connection.channel()

    # the queue that we want to send a "message"
    channel.queue_declare(queue=queue)
    channel.basic_publish(
        exchange='', routing_key=queue, body=json.dumps(message, ensure_ascii=True)
    )
    connection.close()

if __name__ == "__main__":
    #app.run(debug=True, host='127.0.0.1', port=5000)
    app.run(host='0.0.0.0',debug=True)
