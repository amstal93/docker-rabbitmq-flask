#!/usr/bin/env python3

import pymongo

client = pymongo.MongoClient("127.0.0.1:27017")
collection = client["db"]["rabbit"]

for x in collection.find():
    print(x)
